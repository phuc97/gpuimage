//
//  GPUImagePhotoCamera.m
//  GPUImage
//
//  Created by Do Huu Phuc on 12/30/19.
//  Copyright © 2019 Brad Larson. All rights reserved.
//

#import "GPUImagePhotoCamera.h"

@interface GPUImagePhotoCamera () <AVCapturePhotoCaptureDelegate>
{
    AVCapturePhotoOutput *photoOutput;
    NSDictionary *formatPhotoSettings;
    GPUImageOutput<GPUImageInput> *_finalFilterInChain;
    void (^ _block)(NSError *error);
    BOOL isCapturingStillImage;
}


@end

@implementation GPUImagePhotoCamera
{

}

#pragma mark -
#pragma mark Initialization and teardown

- (id)initWithSessionPreset:(NSString *)sessionPreset cameraPosition:(AVCaptureDevicePosition)cameraPosition;
{
    if (!(self = [super initWithSessionPreset:sessionPreset cameraPosition:cameraPosition]))
    {
        return nil;
    }

    /* Detect iOS version < 6 which require a texture cache corruption workaround */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
//    requiresFrontCameraTextureCacheCorruptionWorkaround = [[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending;
#pragma clang diagnostic pop

    isCapturingStillImage = NO;

    [self.captureSession beginConfiguration];

    photoOutput = [[AVCapturePhotoOutput alloc] init];

    [self.captureSession addOutput:photoOutput];

    // Having a still photo input set to BGRA and video to YUV doesn't work well, so since I don't have YUV resizing for iPhone 4 yet, kick back to BGRA for that device
//    if (captureAsYUV && [GPUImageContext supportsFastTextureUpload])
    if (captureAsYUV && [GPUImageContext deviceSupportsRedTextures])
    {
        BOOL supportsFullYUVRange = NO;
        NSArray *supportedPixelFormats = photoOutput.availablePhotoPixelFormatTypes;
        for (NSNumber *currentPixelFormat in supportedPixelFormats)
        {
            if ([currentPixelFormat intValue] == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)
            {
                supportsFullYUVRange = YES;
            }
        }

        if (supportsFullYUVRange)
        {
            formatPhotoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
        }
        else
        {
            formatPhotoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
        }

    }
    else
    {
        captureAsYUV = NO;
        formatPhotoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey];

        [videoOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
    }

    //    photoOutput.highResolutionCaptureEnabled = YES;
    //    photoOutput.isHighResolutionCaptureEnabled = YES;
    //    photoOutput.stillImageStabilizationSupported = YES;
    //    photoOutput.highResolutionCaptureEnabled = YES;

    [self.captureSession commitConfiguration];
    self.jpegCompressionQuality = 0.8;

    return self;
}

- (id)init;
{
    if (!(self = [self initWithSessionPreset:AVCaptureSessionPresetPhoto cameraPosition:AVCaptureDevicePositionBack]))
    {
        return nil;
    }
    return self;
}


- (void)removeInputsAndOutputs;
{
    [self.captureSession removeOutput:photoOutput];
    [super removeInputsAndOutputs];
}

#pragma mark - Private Methods

- (void)capturePhotoProcessedUpToFilter:(GPUImageOutput<GPUImageInput> *)finalFilterInChain withImageOnGPUHandler:(void (^)(NSError *error))block
{
    self.startCapture = NSDate.timeIntervalSinceReferenceDate;
    dispatch_semaphore_wait(frameRenderingSemaphore, DISPATCH_TIME_FOREVER);
    if(isCapturingStillImage){
        block([NSError errorWithDomain:AVFoundationErrorDomain code:AVErrorMaximumStillImageCaptureRequestsExceeded userInfo:nil]);
        return;
    }
    
    isCapturingStillImage = YES;
    
    // Assign to property
    _block = block;
    _finalFilterInChain = finalFilterInChain;
    AVCapturePhotoSettings *photoSetting = [AVCapturePhotoSettings photoSettingsWithFormat:formatPhotoSettings];
    // Setting
//    photoSetting.highResolutionPhotoEnabled = YES;
//    photoSetting.autoStillImageStabilizationEnabled = YES;
    
    [photoOutput capturePhotoWithSettings:photoSetting delegate:self];
}

- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhotoSampleBuffer:(CMSampleBufferRef)photoSampleBuffer previewPhotoSampleBuffer:(CMSampleBufferRef)previewPhotoSampleBuffer resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings bracketSettings:(AVCaptureBracketedStillImageSettings *)bracketSettings error:(NSError *)error
{
    NSLog(@"start capture didFinishProcessingPhotoSampleBuffer: %f", NSDate.timeIntervalSinceReferenceDate - self.startCapture);
    if(photoSampleBuffer == NULL){
        _block(error);
        return;
    }

    // For now, resize photos to fix within the max texture size of the GPU
    CVImageBufferRef cameraFrame = CMSampleBufferGetImageBuffer(photoSampleBuffer);
    
    CGSize sizeOfPhoto = CGSizeMake(CVPixelBufferGetWidth(cameraFrame), CVPixelBufferGetHeight(cameraFrame));
    CGSize scaledImageSizeToFitOnGPU = [GPUImageContext sizeThatFitsWithinATextureForSize:sizeOfPhoto];
    if (!CGSizeEqualToSize(sizeOfPhoto, scaledImageSizeToFitOnGPU))
    {
        CMSampleBufferRef sampleBuffer = NULL;
        
        if (CVPixelBufferGetPlaneCount(cameraFrame) > 0)
        {
            NSAssert(NO, @"Error: no downsampling for YUV input in the framework yet");
        }
        else
        {
            GPUImageCreateResizedSampleBuffer(cameraFrame, scaledImageSizeToFitOnGPU, &sampleBuffer);
        }

        dispatch_semaphore_signal(frameRenderingSemaphore);
        [_finalFilterInChain useNextFrameForImageCapture];
        [self captureOutput:photoOutput didOutputSampleBuffer:sampleBuffer fromConnection:[[photoOutput connections] objectAtIndex:0]];
        dispatch_semaphore_wait(frameRenderingSemaphore, DISPATCH_TIME_FOREVER);
        if (sampleBuffer != NULL)
            CFRelease(sampleBuffer);
    }
    else
    {
        // This is a workaround for the corrupt images that are sometimes returned when taking a photo with the front camera and using the iOS 5.0 texture caches
        AVCaptureDevicePosition currentCameraPosition = [[videoInput device] position];
        if ( (currentCameraPosition != AVCaptureDevicePositionFront) || (![GPUImageContext supportsFastTextureUpload]))
        {
            dispatch_semaphore_signal(frameRenderingSemaphore);
            [_finalFilterInChain useNextFrameForImageCapture];
            [self captureOutput:photoOutput didOutputSampleBuffer:photoSampleBuffer fromConnection:[[photoOutput connections] objectAtIndex:0]];
            dispatch_semaphore_wait(frameRenderingSemaphore, DISPATCH_TIME_FOREVER);
        }
    }
    
    CFDictionaryRef metadata = CMCopyDictionaryOfAttachments(NULL, photoSampleBuffer, kCMAttachmentMode_ShouldPropagate);
    _currentCaptureMetadata = (__bridge_transfer NSDictionary *)metadata;
    
    _block(nil);
    
    isCapturingStillImage = NO;
    
    _currentCaptureMetadata = nil;
    
}

@end
