//
//  PhotoViewController.h
//  FilterShowcase
//
//  Created by Do Huu Phuc on 12/24/19.
//  Copyright © 2019 Cell Phone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface PhotoViewController : UIViewController
{
    GPUImagePhotoCamera *stillCamera;
    GPUImageOutput<GPUImageInput> *filter, *secondFilter, *terminalFilter;
    UISlider *filterSettingsSlider;
    UIButton *photoCaptureButton;
//    
    GPUImagePicture *memoryPressurePicture1, *memoryPressurePicture2;
}

//- (void)takePhoto;
@end

NS_ASSUME_NONNULL_END
